﻿using UnityEngine;
using System.Collections;

public class EditFigures : MonoBehaviour {

	public Texture move;
	static bool onMove = false;
	static bool move1 = true;
	static bool move2 = false;
	private int number;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton(0)) {
			number = int.Parse (this.transform.name);
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
				if (hit.transform == this.transform && !onMove &&
				    ((MoveFigure.board [number / 10 - 1, number % 10 - 1].parent.name == "Sente" || move1)|| 
				     (MoveFigure.board [number / 10 - 1, number % 10 - 1].parent.name == "Gote" || move2))) 
					ChangeTexture();
				if (onMove && this.transform.GetComponent<Renderer>().material.mainTexture == move)
					MakeMove();
			}
		}
	}

	void OnCollisionEnter (Collision collision) {
		number = int.Parse (this.transform.name);
		MoveFigure.board [number / 10 - 1, number % 10 - 1] = collision.transform; 
	}

	void OnCollisionExit(Collision collision) {
		number = int.Parse (this.transform.name);
		MoveFigure.board [number / 10 - 1, number % 10 - 1] = null;
	}

	void MakeMove () {
		onMove = false;
	}

	void ChangeTexture () {
		number = int.Parse (this.transform.name);
		onMove = true;
		if (MoveFigure.board [number / 10 - 1, number % 10 - 1] != null) {
			int[,] moves = MoveFigure.board [number / 10 - 1, number % 10 - 1].GetComponent<MoveLibrary>().moves;
			if (MoveFigure.board [number / 10 - 1, number % 10 - 1].parent.name == "Sente") {
				if (moves[0,1] == 1 && number / 10 < 8 && number % 10 != 1)
					if (!MyFigures("Sente", number + 19))
						this.transform.parent.FindChild((number + 19).ToString()).GetComponent<Renderer>().material.mainTexture = move;
				if (moves[0,3] == 1 && number / 10 < 8 && number % 10 != 9)
					if (!MyFigures("Sente", number + 21))
						this.transform.parent.FindChild((number + 21).ToString()).GetComponent<Renderer>().material.mainTexture = move;
				if (moves[1,2] == 1 && number / 10 != 9)
					if (!MyFigures("Sente", number + 10))
						this.transform.parent.FindChild((number + 10).ToString()).GetComponent<Renderer>().material.mainTexture = move;
			}
		}
	}

	bool MyFigures(string parent, int number) {
		if (MoveFigure.board [number / 10 - 1, number % 10 - 1] != null)
			if (MoveFigure.board [number / 10 - 1, number % 10 - 1].parent.name == parent)
				return true;
			else 
				return false;
		else
			return false;
	}

}
