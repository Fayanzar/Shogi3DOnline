﻿using UnityEngine;
using System.Collections;

public class PutFigures : MonoBehaviour {

	public GameObject Fuhyou;
	public GameObject Kyousha;
	public GameObject Keima;
	public GameObject Ginshou;
	public GameObject Kinshou;
	public GameObject Kakugyou;
	public GameObject Hisha;
	public GameObject Oushou;
	public GameObject Gyoukushou;

	// Use this for initialization
	void Start () {
		GameObject newFigure;
		if (this.name == "Gote") {
			for (int i = 0; i < 9; i++) {
				newFigure = Instantiate (Fuhyou, new Vector3 (4 - i, 0.6884413f, 2.32f) , Quaternion.Euler(4.382019f, -180, 0)) as GameObject;
				newFigure.name = "Fuhyou"+(i+1).ToString();
				newFigure.transform.SetParent(this.transform);
			}
			newFigure = Instantiate (Kyousha, new Vector3 (4, 0.7f, 4.39f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Kyousha1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kyousha, new Vector3 (-4, 0.7f, 4.39f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Kyousha2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Keima, new Vector3 (3, 0.704f, 4.393f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Keima1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Keima, new Vector3 (-3, 0.704f, 4.393f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Keima2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Ginshou, new Vector3 (2, 0.704f, 4.393f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Ginshou1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Ginshou, new Vector3 (-2, 0.704f, 4.393f), Quaternion.Euler(4.382004f, -180, 0)) as GameObject;
			newFigure.name = "Ginshou2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kinshou, new Vector3 (1, 0.71f, 4.422f), Quaternion.Euler(4.381958f, -180, 0)) as GameObject;
			newFigure.name = "Kinshou1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kinshou, new Vector3 (-1, 0.71f, 4.422f), Quaternion.Euler(4.381958f, -180, 0)) as GameObject;
			newFigure.name = "Kinshou2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kakugyou, new Vector3 (3, 0.71f, 3.422f), Quaternion.Euler(4.381958f, -180, 0)) as GameObject;
			newFigure.name = "Kakugyou";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Hisha, new Vector3 (-3, 0.71f, 3.422f), Quaternion.Euler(4.381958f, -180, 0)) as GameObject;
			newFigure.name = "Hisha";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Oushou, new Vector3 (0, 0.712f, 4.433f), Quaternion.Euler(4.381958f, -180, 0)) as GameObject;
			newFigure.name = "Oushou";
			newFigure.transform.SetParent(this.transform);
		}
		if (this.name == "Sente") {
			for (int i = 0; i < 9; i++) {
				newFigure = Instantiate (Fuhyou, new Vector3 (-4 + i, 0.6884413f, -2.32f) , Quaternion.Euler(4.382019f, 0, 0)) as GameObject;
				newFigure.name = "Fuhyou"+(i+1).ToString();
				newFigure.transform.SetParent(this.transform);
			}
			newFigure = Instantiate (Kyousha, new Vector3 (-4, 0.7f, -4.39f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Kyousha1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kyousha, new Vector3 (4, 0.7f, -4.39f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Kyousha2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Keima, new Vector3 (-3, 0.704f, -4.393f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Keima1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Keima, new Vector3 (3, 0.704f, -4.393f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Keima2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Ginshou, new Vector3 (-2, 0.704f, -4.393f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Ginshou1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Ginshou, new Vector3 (2, 0.704f, -4.393f), Quaternion.Euler(4.382004f, 0, 0)) as GameObject;
			newFigure.name = "Ginshou2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kinshou, new Vector3 (-1, 0.71f, -4.422f), Quaternion.Euler(4.381958f, 0, 0)) as GameObject;
			newFigure.name = "Kinshou1";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kinshou, new Vector3 (1, 0.71f, -4.422f), Quaternion.Euler(4.381958f, 0, 0)) as GameObject;
			newFigure.name = "Kinshou2";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Kakugyou, new Vector3 (-3, 0.71f, -3.422f), Quaternion.Euler(4.381958f, 0, 0)) as GameObject;
			newFigure.name = "Kakugyou";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Hisha, new Vector3 (3, 0.71f, -3.422f), Quaternion.Euler(4.381958f, 0, 0)) as GameObject;
			newFigure.name = "Hisha";
			newFigure.transform.SetParent(this.transform);
			newFigure = Instantiate (Gyoukushou, new Vector3 (0, 0.712f, -4.433f), Quaternion.Euler(4.381958f, 0, 0)) as GameObject;
			newFigure.name = "Gyoukushou";
			newFigure.transform.SetParent(this.transform);
		}
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
