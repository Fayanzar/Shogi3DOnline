﻿using UnityEngine;
using System.Collections;

public class MoveLibrary : MonoBehaviour {
	
	public int[,] moves = new int[5,5]; 
	public bool canTransform = false;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < 5; i++)
			for (int j = 0; j < 5; j++)
				moves[i,j] = 0;
		if (this.tag == "Fuhyou") {
			moves[1,2] = 1;
			canTransform = true;
		}
		if (this.tag == "Ginshou") {
			moves[1,1] = 1;
			moves[1,2] = 1;
			moves[1,3] = 1;
			moves[3,1] = 1;
			moves[3,3] = 1;
			canTransform = true;
		}
		if (this.tag == "Gyokushou" || this.tag == "Oushou") {
			moves[1,1] = 1;
			moves[1,2] = 1;
			moves[1,3] = 1;
			moves[2,1] = 1;
			moves[2,3] = 1;
			moves[3,1] = 1;
			moves[3,2] = 1;
			moves[3,3] = 1;
			canTransform = false;
		}
		if (this.tag == "Hisha") {
			moves[1,2] = 2;
			moves[2,1] = 2;
			moves[2,3] = 2;
			moves[3,2] = 2;
		}
		if (this.tag == "Kakugyou") {
			moves[1,1] = 2;
			moves[1,3] = 2;
			moves[3,1] = 2;
			moves[3,3] = 2;
			canTransform = true;
		}
		if (this.tag == "Keima") {
			moves[0,1] = 1;
			moves[0,3] = 1;
			canTransform = true;
		}
		if (this.tag == "Kinshou") {
			moves[1,1] = 1;
			moves[1,2] = 1;
			moves[1,3] = 1;
			moves[2,1] = 1;
			moves[2,3] = 1;
			moves[3,2] = 1;
			canTransform = false;
		}
		if (this.tag == "Kyousha") {
			moves[1,2] = 2;
			canTransform = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
