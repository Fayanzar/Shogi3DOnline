﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkInit : NetworkBehaviour {
	public static NetworkInit use;

	public class MyMsgType {
		public static short Info = MsgType.Highest + 1;
	}

	public class InfoMessage : MessageBase {
		public byte[] avatar;
		public string email;
		public string password;
		public string startPosition;
		public string endPosition;
		public string goal;
	}

	NetworkManager manager;
	NetworkClient myClient;
	public Text email;
	public Text password;
	public string IP;

	void Awake () {
		myClient = new NetworkClient ();
		myClient.Connect (IP, 7777);
	}

	private byte[] GetBytes(string str) {
		byte[] bytes = new byte[str.Length * sizeof(char)];
		System.Buffer.BlockCopy (str.ToCharArray (), 0, bytes, 0, bytes.Length);
		return bytes;
	}

	private string GetString(byte[] bytes) {
		System.Text.StringBuilder hex = new System.Text.StringBuilder (bytes.Length * 2);
		foreach (byte b in bytes) 
			hex.AppendFormat ("{0:x2}", b);
		return hex.ToString ();
	}
	
	public void SendByClick () {
		InfoMessage msg = new InfoMessage ();
		myClient.Send (MyMsgType.Info, msg);
	}

	public void LogIn () {
		System.Security.Cryptography.SHA512Managed sha = new System.Security.Cryptography.SHA512Managed ();
		string pass = GetString (sha.ComputeHash (GetBytes (password.text)));
		InfoMessage msg = new InfoMessage ();
		msg.goal = "login";
		msg.email = email.text;
		msg.password = pass;
		myClient.Send (MyMsgType.Info, msg);
	}

	public void ChangeAvatar (byte[] bytes) {

	}

}
